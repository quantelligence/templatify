Templatify
==========
## Standard practice ##

The purpose of this library is to allow for client-side in-place templating.

The standard practice for using template is to create a template at one place, then render it into some DOM element using javascript code. The standard example is something like this

```
#!html
<!-- This is where the template will be rendered -->
<div id=placeholder></div>


<!-- This is where the template gets defined - standard practice -->
<script type='text/template' id='mytemplate' >
     I am going to say: <%=firstword%> <%=secondword%>
</script>


<script>
    // and now the rendering part
    data = { firstword : 'Hello', secondword : 'World' };
    $("#placeholder").html( _.template( $("#mytemplate").html(), data );
</script>
```

The issue with this is that it requires the programmer to keep track of **which placeholder** consumes **which data**.


## Why Templatify? ##

The classical use of templates (i.e. using data that is passed inside javascript) remains vital when actual data is involved, but when the variables inside the templates are static bits of html, it is preferable to maintain proximity between where the template is displaid and what goes inside.

The Templatify function helps creating a series of HTML tags which will replace the placeholder **and** carry the data. The main benefit will be to define shortcuts for commonly-used blocks of complicated html (which are a frequent feature of frameworks like bootstrap), with minimal (and completely generic) javascript.

## Example usage ##

See example below


```
#!html
<html><body>

	<!-- this is one use (instance) of the template 
	attributes:
		inside the first div
			template: required to specify what template to use
			somevariable, ... : are user-defined variables
		inside the inner div: user-defined variables that are longer or comprise html
				 need to use <div name=[nameofvariable]>...</div>
	-->

	<div template='mytemplate' somevariable='howdy' someothervariable='world'>	
		<div name=somethirdvariable>
			a <b>block</b> of html
		</div>
	</div>

	<!-- this is another use of the template -->
	<div template='mytemplate' someothervariable='world'>
		<div name=somethirdvariable>
			a <b>block</b> of html
		</div>
	</div>

</body></html>


<!-- This is where the template gets defined - standard practice -->
<!-- note the 'somevariable' attribute which sets the default value if it is not defined in the template instance -->
<script type='text/template' id='mytemplate' somevariable='hello' someundefinedvariable='huh?'>
	<p>Here we can output <%=somevariable%> <%=someothervariable%> as well as <%=somethirdvariable%>. 
	We can also use variables that have not been defined such as '<%=someundefinedvariable%>'
</script>


<script src='http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.4/underscore-min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.1/jquery.min.js'></script>
<script src='templatify.js'></script>

<script>

$(document).ready( function() {
	$("[template]").each( Templatify )

	// or you could put them all in a class
	// or render just one specific template $("[template='mytemplate']")
})

</script>
```

If you find this useful please let me know!
