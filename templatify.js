/* 
    Document   : templatify.js
    Created on : May-2013
    Author     : Joel Horowitz
    Description:

	In-place HTML template function for javascript

*/


$.fn.Attributes = function() { 

	var data = {}
	var attrs = $(this).get(0).attributes;
	// get variables from inside div
	$(attrs).each(function(i,item)
	      {
	           data[ item.nodeName ] = item.nodeValue;                  
	      });
	return data;
}

Templatify = function() {
	// get data from inside the div
	var data = $(this).Attributes()

	// now extend with data from individual divs
	$(this).find('div').each( function(){
		node = this.attributes.getNamedItem('name')
		if ( node != null )	// i.e. ignore other divs
			data[node.value] = this.innerHTML
	})
	
	var template = $("#" + data.template )

	// get defaults as defined in template
	var defaults = $(template).Attributes()
	_.extend( defaults, data ) 

	templatehtml = "<!-- Templatified '<%=template%>' -->" +
			template.html()
		       "<!-- Templatified '<%=template%>' (end) -->"
	
	var html = _.template( templatehtml, defaults )


	$(this).replaceWith( html )
	return this
}

